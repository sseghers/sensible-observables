import { BehaviorSubject, ReplaySubject, Subject, of, from } from 'rxjs';
import {
  finalize,
  map,
  take,
  reduce,
  distinct,
  distinctUntilChanged,
  tap,
} from 'rxjs/operators';
import {expect} from 'chai';
// import sinon from 'sinon';

import {log} from './helpers';
import {
  IntervalFactory,
  getNumbersSource,
  getMultipleTypeSource,
} from './sources';


// A goal is to create observable pipelines that achieve the objective over manually 
// writing logic within a subscription.

describe('An interval based source, IntervalFactory, can be used and', () => {
  it('should contain the iterator values in intervals', () => {
    const [source$, clear] = IntervalFactory()();

    source$.pipe(take(5), clear).subscribe(val => {
      log(val);
    });
  });

  it('should contain the stateful iterator values in intervals', () => {
    const iterator = (previous = 0) => previous + 1;
    const [source$, clear] = IntervalFactory(Subject, 500, true)(iterator);
    source$.pipe(take(5), clear).subscribe(val => {
      log(val);
    });
  });
});

describe('A series of single data sources', () => {
  describe('getNumbersSource', () => {
    it('should provide numbers', () => {
      getNumbersSource().pipe(reduce((numbersArray, current) => {
        numbersArray.push(current);
        return numbersArray;
      }, [])).subscribe(resultArray => {
        expect(resultArray.length).eq(100);
        expect(resultArray.every(val => typeof val === 'number')).is.true;
      });
    });
  });

  describe('getMultipleTypeSource', () => {
    it('should provide a mixture of types', () => {
    });

    it('should only capture distinct values', () => {
      getMultipleTypeSource().pipe(distinct()).subscribe();
    });

    it('should only capture sequential distinct values', () => {
      const originalStream = [];
      getMultipleTypeSource()
      .pipe(
        tap(val => originalStream.push(val)),
        distinctUntilChanged(),
        finalize(() => log(originalStream))
      )
      .subscribe(val => log('modified stream', val));
    });
  });
});
