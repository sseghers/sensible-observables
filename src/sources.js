import { Subject, from } from 'rxjs';
import { finalize } from 'rxjs/operators';

import {StateHelpers} from './ouroboros/state/index';
import {log} from './helpers';

const {ActionTypes} = StateConstants;

function getRandomInt(max) {
  return Math.floor(Math.random() * Math.floor(max));
}

function IntervalFactory(
  SubjectType = Subject,
  delay = 500,
  stateful = false,
) {
  return function CreateInterval(iterator) {
    const source$ = new SubjectType();

    let internalIterator = iterator;
    let state;
    if (stateful) {
      internalIterator = () => {
        state = iterator(state);
        return state;
      }
    }
    const intervalInstance = setInterval(() => {
      source$.next(internalIterator());
    }, delay);

    return [source$, finalize(() => clearInterval(intervalInstance))];
  }
}

function getNumbersSource(amount = 100) {
  return from(new Array(amount).fill().map((_, index) => index));
}


function getMultipleTypeSource() {
  const sourceValues = ['string type', true, 42];
  const getRandomIndex = getRandomInt.bind(this, 3);

  const multiTypeStream = [];
  for(let i = 0; i < 9; i++) {
    multiTypeStream.push(sourceValues[getRandomIndex()]);
  }

  return from(multiTypeStream);
}

function eventCreator(actionType, action) {
  function createEventWithAction(action) {
    return {
      action,
      type: actionType,
    }
  }
  if (action === undefined) {
    return createEventWithAction;
  }
  return createEventWithAction(action);
}

function splitAction(partialEvent, action) {
  return action.split('').map((_, index, source) => {
    return partialEvent(source.slice(0, index + 1).join(''));
  }, []);
}

function mapSeriesToEvents(actionSeries) {
  return actionSeries.flatMap(([type, action, transformer]) => {
    if(transformer) {
      return transformer(eventCreator(type), action);
    }

    return eventCreator(type, action);
  })
}

function asCompoundAction(property, split = true) {
  function nestProperty(action) {
    return {
      [property]: action,
    }
  }
  return function createCompoundAction(partialEvent, action) {
    if(!split) {
      return partialEvent(nestProperty(action));
    }
    return splitAction(partialEvent, action).reduce((eventAcc, currEvent) => {
      eventAcc.push({
        ...currEvent,
        'action': nestProperty(currEvent.action), 
      });
      return eventAcc;
    }, []);
  }
}

const flows = {
  homePage: {
    happyFlow: mapSeriesToEvents([
      [ActionTypes.global.init, null],
      [ActionTypes.home.upsertLogIn, 'User101', asCompoundAction('username')],
      [ActionTypes.home.upsertLogIn, 'SecretPassword', asCompoundAction('password')],
      [ActionTypes.home.logIn, null],
      [ActionTypes.home.upsertSearchText, 'That One Hip Series', splitAction],
      [ActionTypes.home.searchText, null],
      [ActionTypes.home.queueVideo, 'xID01'],
    ]),
  }
}

export { IntervalFactory, getNumbersSource, getMultipleTypeSource };


