import {
  combineLatest,
  identity,
  interval,
  Subject,
  timer,
  zip,
} from 'rxjs';
import { 
  debounce,
  map,
  pluck, 
  take,
  takeUntil,
  tap,
} from 'rxjs/operators';
import {expect} from 'chai';

import {log} from '../helpers';
import {ActionTypes} from './state/stateConstants';
import Ouroboros from './system/ouroboros';
import UserModule, {userModuleProperties} from './modules/userModule';
import {machineStateKey} from './modules/helpers';

// Ouroboros config
const StateContainer = Ouroboros([UserModule]);

const {
  name: userNamespace,
  userStates,
} = userModuleProperties;

const eventFactory = type => action => ({
  action,
  type,
});

const createUserUpsert = eventFactory(ActionTypes.home.upsertLogIn);
const createQueueVideo = eventFactory(ActionTypes.home.queueVideo);
const createTextSearch = eventFactory(ActionTypes.home.searchText);

function alternateDebouncedSource(
  updater,
  rawSource$,
  storeValue$,
  control$,
  threshold = 1000) {
  const alternatingSource$ = new Subject();
  const debouncedUpdate = rawSource$.pipe(debounce(() => timer(threshold)), updater).subscribe();

  rawSource$
  .pipe(takeUntil(control$))
  .subscribe({
    next: (rawValue) => {
      alternatingSource$.next(rawValue);
    },
    complete: () => {
      alternatingSource$.complete();
      debouncedUpdate.unsubscribe();
    }
  });
  return alternatingSource$;
}
describe.skip('Some user events, like text typing, needlessly cause frequent updates to be sent to the state management system', () => {
  beforeEach(() => {
    UserModule.reset();
  });

  it('should call the provided callback based on the threshold and allow continued updates', () => {
    const [appState$, consume] = StateContainer({
      derivations: [{
        [userNamespace]: [identity],
      }],
    });
    const dispatchTypeEvent = tap(val => consume(createUserUpsert(val)));
    const stringByInterval = {
      0: 'U',
      1: 'Us',
      2: 'Use',
      3: 'User',
      4: 'Usern', 
      5: 'Userna',
      6: 'Usernam',
      7: 'Username',
      8: 'Username7',
      9: 'Username77',
    }
    const mockTypeEventSource$ = interval(500).pipe(
      map(intervalValue => stringByInterval[intervalValue]),
      take(10));
    const componentStatus$ = new Subject();
    const userName$ = appState$.pipe(pluck(userNamespace, 'username'));
    const componentValue$ = alternateDebouncedSource(
      dispatchTypeEvent,
      mockTypeEventSource$,
      userName$,
      componentStatus$,
      );

    const appStateUpdates = [];
    const byPassUpdates = [];
    userName$.subscribe(next => {
      appStateUpdates.push(next)
    });
    componentValue$.subscribe({
      next: byPassValue => {
        byPassUpdates.push(byPassValue);
        if(byPassValue === stringByInterval[9]) {
          componentStatus$.next();
        }
      },
      complete: () => {
        expect(byPassUpdates).to.have.members(Object.values(stringByInterval));
        expect(appStateUpdates).to.have.members(['', stringByInterval[9]]);
      }
    });
  });
});

describe.skip('The system must wait until the user\'s request has completed', () => {
  beforeEach(() => {
    UserModule.reset();
  });

  it('should broadcast states following a typical video fetching', () => {
    const [appState$, consume] = StateContainer({
      derivations: [{
        [userNamespace]: [identity],
      }],
    });
    const expectedVideoId = 'xid1234abcd';
    const actualStates = [];
  
    appState$.pipe(
      pluck(userNamespace),
      map(userModule => ({
        machineState: userModule[machineStateKey],
        queuedVideo: userModule.queuedVideo,
        currentVideo: userModule.currentVideo,
      })),
      take(3))
      .subscribe({
      next: nextState => {
        actualStates.push(nextState);
      },
      complete: () => {
        expect(actualStates).to.have.deep.members([{
          machineState: userStates.READY,
          queuedVideo: {},
          currentVideo: {},
        },
        {
          machineState: userStates.QUEUEING,
          queuedVideo: {
            id: expectedVideoId
          },
          currentVideo: {},
        },
        {
          machineState: userStates.PLAYING,
          queuedVideo: {},
          currentVideo: {
            id: expectedVideoId
          },
        }]);
      }
    });

    consume(createQueueVideo(expectedVideoId));
  });
});

describe.skip('We want to cancel an existing request after an action has occurred that creates a new one', () => {
  beforeEach(() => {
    UserModule.reset();
  });

 it('should only act on the most recent request while cancelling the previous', () => {
  const [appState$, consume] = StateContainer({
    derivations: [{
      [userNamespace]: [identity],
    }],
  });
  let actualSearchResults = [];
  appState$.pipe(
    pluck(userNamespace, 'searchResults'),
    take(2)
    ).subscribe({
      next: nextResults => {
        actualSearchResults = actualSearchResults.concat(nextResults);
      },
      complete: () => {
        expect(actualSearchResults).to.have.members(['The Stones are not Too Busy']);
      }
    });
  
  consume(createTextSearch('The'));
  consume(createTextSearch('The Stones'));
 });
});
