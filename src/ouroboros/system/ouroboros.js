import {BehaviorSubject, ReplaySubject, Subject} from 'rxjs';
import {multicast} from 'rxjs/operators';

import { log } from '../../helpers';
import {
  applyCombination,
  applyDerivations,
} from '../state/helpers';

/*
  Usage

  const StateContainer = Ouroboros([UserModule]);

  StateContainer({
    derivations: [
    {
      [userNamespace]: [map(userState => ({username: userState.username, lastVideo: userState.lastVideo}))],
    },
    derivedState => {
      const {[userNamespace]: userModule} = derivedState;
      const {
        lastVideo,
        username,
      } = userModule;
      const greeting = lastVideo.marker ? `Welcome back, ${username}. Resume play of ${lastVideo.title}?` : `Welcome, ${username}`;
      return {
        ...userModule,
        greeting,
      }
    }
  ],
});

*/

function Ouroboros(modules) {
  if(!Array.isArray(modules) || modules.length <= 0) {
    throw Error('No modules provided. Ouroboros is meant to be configured with state responsible modules.');
  }

  let aggregatedState$;
  let aggregatedStateReader$ = new ReplaySubject(1);
  const eventStream$ = new Subject();
  // Previously I used {} as the initial state...that revealed itself to be a problem in combining with event source.
  // Instead we'll start with a placeholder subject, then hook that into the real initial state via multicasting.
  aggregatedState$ = new BehaviorSubject(modules.reduce((aggState, stateModule) => {
    aggState[stateModule.namespace] = stateModule.start(
      aggregatedStateReader$,
      eventStream$);
    return aggState;
  }, {}));
  aggregatedState$.pipe(multicast(aggregatedStateReader$)).connect();

  return function createConnection(config) {
    const {
      derivations,
    } = config;

    const consume = event => {
      eventStream$.next(event);
    }
    
    if(derivations.length === 0) {
      return [aggregatedStateReader$, consume];
    }

    const moduleDerivations = derivations[0];
    const combinedDerivations = derivations[1];

    const appliedDerivations = applyDerivations(moduleDerivations, aggregatedStateReader$);
    let finalDerivation = applyCombination(appliedDerivations, combinedDerivations);

    return [finalDerivation, consume];
  }
}

export default Ouroboros

