import {BehaviorSubject} from 'rxjs';

function ModuleFactory(config) {
  const module = (function createModule(moduleConfig) {
    /**
      Properties
    */
    const moduleName = moduleConfig.name;
    let innerModuleState$, subscriptions = null;

    /**
      Dispatcher
    */
    const dispatch = newState => innerModuleState$.next(newState);

    /**
      Module creation
    */
    return {
      dispose: function dispose() {
        subscriptions.forEach(subscription => {
          moduleConfig.dispose(subscription);
          subscription.unsubscribe();
        });
        innerModuleState$.complete();
      },
      namespace: moduleName,
      reset: function() {
        dispatch(moduleConfig.initialState);
      },
      start: function start(appState$, eventStream$) {
        if(innerModuleState$) {
          return innerModuleState$.asObservable();
        }

        innerModuleState$ = new BehaviorSubject({
          ...moduleConfig.initialState,
        });

        subscriptions = moduleConfig.reactions
          .map(reaction => reaction(dispatch, appState$, eventStream$));

        return innerModuleState$.asObservable();
      }
    }
  })(config);
  return module;
} 

export default ModuleFactory;
