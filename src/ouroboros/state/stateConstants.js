const ActionTypes = {
  home: {
    logIn: 'HOME_LOG_IN',
    logOut: 'HOME_LOG_OUT',
    queueVideo: 'HOME_QUEUE_VIDEO',
    register: 'HOME_REGISTER',
    requestVideo: 'HOME_REQUEST_VIDEO',
    searchGenre: 'HOME_SEARCH_GENRE',
    searchText: 'HOME_SEARCH_TEXT',
    upsertLogIn: 'HOME_UPSERT_LOG_IN',
    videoLoaded: 'HOME_VIDEO_LOADED',
  },
};

export {
  ActionTypes
};
