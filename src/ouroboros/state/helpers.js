import {
  BehaviorSubject,
  combineLatest,
  Subject,
  merge,
  from,
  of,
  identity,
  zip
} from 'rxjs';
import {
  concat,
  filter,
  pluck,
  map,
  mergeMap,
  mergeScan,
  sample,
  tap,
  take,
  withLatestFrom
} from 'rxjs/operators';
import { log } from "../../helpers";
import {ActionTypes} from './stateConstants';


function moduleByNamespace(namespace) {
  return mergeMap(source$ => source$[namespace]);
};

function mapStreamOver(transformations) {
  return function(source$) {
    return transformations.map((transformation) => {
      return transformation(source$);
    }, {});
  }
}

function feedDerivation([namespace, derivation]) {
  return function mergeDerivation(state$) {
    return state$.pipe(
      mergeMap(appState =>
        of(appState[namespace].pipe(...derivation, map(derivedState => [namespace, derivedState])))));
  }
}

function applyDerivations(moduleDerivations, source$) {
  const mergeDerivations = Object.entries(moduleDerivations).map(feedDerivation);
  return source$.pipe(mapStreamOver(mergeDerivations));
}

function applyCombination(appliedDerivations, finalDerivation = identity) {
  return merge(...appliedDerivations.map(moduleSlice => {
    return moduleSlice.pipe(mergeMap(map(([namespace, source]) => ({
      [namespace]: source,
    }))))
  })).pipe(map(finalDerivation));
}


export {
  applyCombination,
  applyDerivations,
  moduleByNamespace,
  feedDerivation,
  mapStreamOver,
}
