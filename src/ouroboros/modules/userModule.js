import {
  Observable,
} from 'rxjs';
import {
  filter,
  map,
  mergeMap,
  take,
  switchMap,
} from 'rxjs/operators';

import ModuleFactory from '../system/moduleFactory';
import {StateConstants, StateHelpers} from '../state/index';
import { log } from '../../helpers';
import {
  createRequestObservable,
  getReactionContext,
  getTransitions,
  machineStateKey,
  makeModuleStates,
} from './helpers';

const {ActionTypes} = StateConstants;
const {
  moduleByNamespace,
} = StateHelpers;


const moduleName = 'USER_MODULE';
const userModuleByNamespace = moduleByNamespace(moduleName);
const userStates = makeModuleStates(moduleName, [
  'START',
  'READY',
  'QUEUEING',
  'PLAYING',
]);
const stateMachineConfig = {
  [userStates.START]: [],
  [userStates.READY]: [
    ActionTypes.home.upsertLogIn,
    ActionTypes.home.queueVideo,
    ActionTypes.home.searchText,
  ],
  [userStates.QUEUEING]: [ActionTypes.home.videoLoaded],
  [userStates.PLAYING]: [ActionTypes.home.queueVideo],
};

/* 
  This was a head scratcher. The real observable used here, the observable of context$ piping
  from mergeMap to the inner observable of module$, was left open meaning it continually
  emit the values sent the subject from the following subscriber...hence an infinite loop. We take
  the first value, which is just the changed dispatched, and then complete instead.
*/
const reactions = [
  function onUserLoginUpsert(dispatch, appState$, eventStream$) {
    const context$ = getReactionContext(
      context => {
        const {event} = context;
        const module$ = context[moduleName];
        return module$.pipe(map(moduleState => ({event, state: {...moduleState}})), take(1));
      },
      getTransitions(userModuleByNamespace, stateMachineConfig, appState$),
      ActionTypes.home.upsertLogIn,
      appState$,
      eventStream$
    );
    
    return context$.subscribe(({event, state}) => {
      dispatch({
        ...state,
        username: event.action,
      });
    });
  },
  function onVideoQueue(dispatch, appState$, eventStream$) {
    const context$ = getReactionContext(
      context => {
        const {event} = context;
        const module$ = context[moduleName];
        return module$.pipe(map(moduleState => ({event, state: {...moduleState}})), take(1));
      },
      getTransitions(userModuleByNamespace, stateMachineConfig, appState$),
      ActionTypes.home.queueVideo,
      appState$,
      eventStream$
    );
    return context$.subscribe(({event, state}) => {
      dispatch({
        ...state,
        [machineStateKey]: userStates.QUEUEING,
        queuedVideo: {
          id: event.action
        }
      });
    });
  },
  function onRequestVideo(dispatch, appState$, eventStream$) {
    return appState$.pipe(
      mergeMap(module => {
        return module[moduleName].pipe(
          filter(userModule => userModule[machineStateKey] === userStates.QUEUEING),
        )
      }),
    )
    .subscribe(user => {
      const sendVideoRequest = args => Promise.resolve({
        id: args.queuedVideo.id,
      });
      createRequestObservable(sendVideoRequest, user).subscribe(result => {
        dispatch({
          ...user,
          queuedVideo: {},
          currentVideo: {
            ...result,
          },
          [machineStateKey]: userStates.PLAYING,
        })
      });
    });
  },
  function onTextSearch(dispatch, appState$, eventStream$) {
    function matchResults(searchText) {
      return {
        results: [
          'The Last',
          'The Tales of Ainran',
          'The Beginning of the Story',
          'The Stones are not Too Busy',
          'The Lamp',
          'The Desk',
          'The Thing that said Moo',
        ].filter(title => title.startsWith(searchText)),
        text: searchText,
      }
    }

    function createRequest(endpoint, responseHandler, timeout = 200) {
      return new Observable(subscriber => {
        setTimeout(() => {
          subscriber.next(responseHandler(endpoint()));
          subscriber.complete();
        }, timeout);
      })
    }

    const context$ = getReactionContext(
      context => {
        const {event} = context;
        const module$ = context[moduleName];
        return module$.pipe(map(moduleState => ({event, state: {...moduleState}})), take(1));
      },
      getTransitions(userModuleByNamespace, stateMachineConfig, appState$),
      ActionTypes.home.searchText,
      appState$,
      eventStream$
    );

    return context$.pipe(switchMap((({event, state}) => {
      return createRequest(
        matchResults.bind(this, event.action),
        matchingData => ({
          event: matchingData,
          state,
        })
      );
    }))).subscribe(({event, state}) => {
      dispatch({
        ...state,
        searchResults: [
          ...event.results,
        ]
      });
    });
  }
];

// Normally the module would start in "START" after performing initialization logic, but for simplicity we skip to "READY"
const userModuleConfig = {
  dispose: () => null,
  initialState: {
    [machineStateKey]: userStates.READY,
    currentVideo: {},
    id: '',
    password: '',
    searchResults: [],
    queuedVideo: {},
    username: '',
  },
  name: moduleName,
  reactions,
};

export const userModuleProperties = {
  initialState: userModuleConfig.initialState,
  name: moduleName,
  userModuleByNamespace,
  userStates,
};
export default ModuleFactory(userModuleConfig);
