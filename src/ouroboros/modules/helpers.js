import {
  combineLatest,
  Subject,
} from 'rxjs';
import {
  filter,
  map,
  mergeMap,
  pluck,
  sample,
  switchMap,
} from 'rxjs/operators';
import { log } from '../../helpers';

export const machineStateKey = 'MACHINE_STATE';

const makeModuleStates = (domain, stateKeys) => {
  return stateKeys.reduce((state, stateKey) => {
    state[stateKey] = `${domain}_${stateKey}`;
    return state;
  }, {});
}

function filterByTransitions(config, state$) {
  return function(event$) {
    return state$.pipe(
      pluck(machineStateKey),
      map(currentState => config[currentState]),
      switchMap(transitions => event$.pipe(filter(event => transitions.includes(event.type)))),
    )
  }
}

function getTransitions(
  moduleByNamespace,
  machineConfig,
  appState$
) {
  return filterByTransitions(
    machineConfig,
    appState$.pipe(moduleByNamespace),
  )
}

function getReactionContext(
  reactionProjection,
  transitions$,
  eventType,
  appState$,
  eventStream$
) {
  const filteredEvents$ = eventStream$.pipe(
    filter(event => event.type === eventType),
    transitions$,
    map(event => ({event})),
  );

  return combineLatest([appState$, filteredEvents$])
    .pipe(
      sample(filteredEvents$),
      map(combinedState => {
      // Convert to object
      return combinedState.reduce((reducedState, slice) => {
        return {
          ...reducedState,
          ...slice,
        };
      }, {});
    }),
    mergeMap(reactionProjection));
}

function createRequestObservable(
  asyncRunner,
  args,
  subject = null,
) {
  let requestSubject = subject
    ? subject
    : new Subject();
  
  asyncRunner(args)
    .then(success => {
      requestSubject.next(success);
    })
    .catch(error => {
      requestSubject.next(error);
    })
    .finally(() => {
      if(!subject) {
        requestSubject.complete();
      }
    });
  return requestSubject;
}

export {
  createRequestObservable,
  filterByTransitions,
  getReactionContext,
  getTransitions,
  makeModuleStates,
};
