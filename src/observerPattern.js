const observerA = {
  name: 'observerA',
  next: function(nextState) {
    this.reportState(nextState);
  },
  reportState: function reportState(nextState) {
    console.log('observerA:', nextState);
  },
}

const observerB = {
  name: 'observerB',
  next: function(nextState) {
    this.updateModule(nextState);
  },
  updateModule: function(nextState) {
    // do things with next iteration of state 
    console.log('observerB:', nextState);
  }
}

function Subject() {
  this.observers = [];

  this.registerObserver = function registerObserver(newObserver) {
    this.observers.push(newObserver);
  }

  this.removeObserver = function removeObserver(deleteObserver) {
    this.observers = this.observers.filter(observer => observer !== deleteObserver);
  }

  this.pushNext = function pushNext(iteration) {
    this.observers.forEach(observer => observer.next(iteration));
  }
}

// Create our subject
const testSubject = new Subject();

testSubject.registerObserver(observerA);

testSubject.pushNext('apples');
// observerA: apples

testSubject.registerObserver(observerB);

testSubject.pushNext('bananas');
// observerA: bananas
// observerB: bananas

testSubject.removeObserver(observerB);

testSubject.pushNext('oranges');
// observerA: oranges





function StatefulSubject(initialState) {
  this.observers = [];
  this.currentState = initialState;

  this.registerObserver = function registerObserver(newObserver) {
    this.observers.push(newObserver);
    newObserver.next(this.currentState);
  }

  this.removeObserver = function removeObserver(deleteObserver) {
    this.observers = this.observers.filter(observer => observer !== deleteObserver);
  }

  this.pushNext = function pushNext(iteration) {
    this.currentState = iteration;
    this.observers.forEach(observer => observer.next(iteration));
  }
}

const stateSubject = new StatefulSubject('apples');

stateSubject.registerObserver(observerA);
// observerA: apples

stateSubject.pushNext('bananas');
// observerA: bananas

stateSubject.registerObserver(observerB);
// observerB: bananas

stateSubject.pushNext('grapes');
// observerA: grapes
// observerB: grapes

stateSubject.removeObserver(observerB);

stateSubject.pushNext('oranges');
// // observerA: oranges
